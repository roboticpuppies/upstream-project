# upstream-project

Example of project that uses multi-project pipelines.

It consists in:

- a build stage that generates an artifact when running on `master`
- a set of tests where one of them (`system_tests`) runs as a downstream multi-project pipeline
    - pipeline on `system-tests` is triggered using `strategy:depend` because a failure should cause the main pipeline to fail
    - when the main pipeline is triggered against a branch (not `master`) it would download the latest artifact from `master`
- a `deploy` stage containing a deployment job and an async (multi-project) pipeline that collects some metrics. This pipeline
  is triggered without `strategy:depend` because we want it not to affect the main pipeline given that it's being deployed. 

## How to use it

1. Run a pipeline on `master`. This will run the `build` job which generates some artifacts. These artifacts are only generate when running the pipeline on `master`
1. Create a new branch and make a change (optionally create a merge request). When committing the change, note that a `system-test-with-artifacts` job runs, which
   uses the latest artifacts from `master` (previously generated). You can inspect the job log and see that the content of the artifact matches the one in (1).

## Untuk ngetrigger commit
